﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuCtrl : MonoBehaviour {

    /* Funcion para cargar escena mediante un boton*/
	public void LoadScene(string sceneName)
	{
		SceneManager.LoadScene (sceneName);
	}

    /* Sale del juego*/
	public void Exit()
	{
		Application.Quit ();
		Debug.Log ("Ha salido del juego");
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Detector : MonoBehaviour {
	public Text outputF;
	public GameObject panel, boton;
	public Button siguiente;
	public string obj = null;
	public int cant = 0, cant1 = 0, cant2 = 0;
	public static Detector instDetector = null;

	// Use this for initialization
	void Start () {
		instDetector = this;
	}

	public void OnTriggerEnter2D(Collider2D other){
		Button btn = siguiente.GetComponent<Button>();
		if(other.name == "NPC_inicial"){
			Dialogos1.instDialogo.DialogoInicial ();
		}else if(other.name == "LoboIni_0" || other.name == "LoboIni_1" || other.name == "LoboIni_2" || 
			other.name == "MamutIni_0" || other.name == "MamutIni_1" || other.name == "MamutIni_2"){
			Time.timeScale = 0;
			outputF.text = "Para pasar esta misión deberás atrapar a los animales que puedas, ¡Adelante!";
			boton.SetActive (true);
			panel.SetActive (true);
			btn.onClick.RemoveAllListeners();
			btn.onClick.AddListener (siguienteAnimal);
		}else if(other.name == "Lobo_0" || other.name == "Lobo_1" || other.name == "Lobo_2"){
			obj = other.name;
			InstanciaAnimales.instAnimales.destruyeLobo ();
		}else if( other.name == "Mamut_0" || other.name == "Mamut_1" || other.name == "Mamut_2"){
			obj = other.name;
			InstanciaAnimales.instAnimales.destruyeMamut ();
		}else if(other.name == "Ciervo_0" || other.name == "Ciervo_1" || other.name == "Ciervo_2"){
			obj = other.name;
			InstanciaAnimales.instAnimales.destruyeCiervo ();
		}else if(other.name == "Rino_0" || other.name == "Rino_1" || other.name == "Rino_2"){
			obj = other.name;
			InstanciaAnimales.instAnimales.destruyeRino ();
		}else if(other.name == "Bisonte_0" || other.name == "Bisonte_1" || other.name == "Bisonte_2"){
			obj = other.name;
			InstanciaAnimales.instAnimales.destruyeBisonte ();
		}else if(other.name == "NPC_nomada"){
			var hacha1 = GameObject.Find ("Hacha1");
			var hacha2 = GameObject.Find ("Hacha2");
			var cuerda1 = GameObject.Find ("Cuerda1");
			var cuerda2 = GameObject.Find ("Cuerda2");
			var navaja1 = GameObject.Find ("Navaja1");
			var navaja2 = GameObject.Find ("Navaja2");
			var red1 = GameObject.Find ("Red1");
			var red2 = GameObject.Find ("Red2");
			var aguja1 = GameObject.Find ("Aguja1");
			var aguja2 = GameObject.Find ("Aguja2");
			if ((hacha1 == null || hacha2 == null) && (cuerda1 == null || cuerda2 == null) && (navaja1 == null || navaja2 == null) && (red1 == null || red2 == null) && (aguja1 == null || aguja2 == null)) {
				outputF.text = "Hola, veo que traes muchas cosas, son muy utiles. Para terminar con esta mision, solo tendrás que responder a 5 preguntas correctamente. Tienes 3 oportunidades para "
					+ "responder mal, si las sobrepasas la misión se reinicia.";
				boton.SetActive (true);
				panel.SetActive (true);
				btn.onClick.RemoveAllListeners ();
				btn.onClick.AddListener (Dialogos2.instDialogo2.siguienteNPC_nomada);
			} else {
				outputF.text = "Al parecer no has encontrado los 5 objetos minimos, recuerda que estos son un hacha, una navaja, una cuerda, una red y una aguja. Sigue buscando.";
				boton.SetActive (false);
				panel.SetActive (true);
			}
		}else if (other.name == "Hacha1" || other.name == "Hacha2") {
			Dialogos2.instDialogo2.hacha ();
		}else if(other.name == "Cuerda1" || other.name == "Cuerda2"){
			Dialogos2.instDialogo2.cuerda ();
		}else if(other.name == "Navaja1" || other.name == "Navaja2"){
			Dialogos2.instDialogo2.navaja ();
		}else if(other.name == "Red1" || other.name == "Red2"){
			Dialogos2.instDialogo2.red ();
		}else if(other.name == "Aguja1" || other.name == "Aguja2"){
			Dialogos2.instDialogo2.aguja ();
		}else if(other.name == "NPC_nomada1"){
			Dialogos2.instDialogo2.dialogoInicial ();
		}else if(other.name == "NPC_agricultura"){
			Dialogos3.instDialogo3.introduccion1 ();
		}else if(other.name == "CampoMaiz"){
			Dialogos3.instDialogo3.maiz ();
		}else if(other.name == "CampoZanahoria"){
			Dialogos3.instDialogo3.zanahoria ();
		}else if(other.name == "CampoAguacate"){
			Dialogos3.instDialogo3.aguacate ();
		}else if(other.name == "CampoUva"){
			Dialogos3.instDialogo3.uva ();
		}else if(other.name == "CampoCebada"){
			Dialogos3.instDialogo3.cebada ();
		}else if(other.name == "CampoChile"){
			Dialogos3.instDialogo3.chile ();
		}else if(other.name == "CampoFrijol"){
			Dialogos3.instDialogo3.frijol ();
		}else if(other.name == "CampoCol"){
			Dialogos3.instDialogo3.col ();
		}else if(other.name == "CampoAmaranto"){
			Dialogos3.instDialogo3.amaranto ();
		}else if(other.name == "NPC_agricultura1"){
			Dialogos3.instDialogo3.ayuda1 ();
		}else if(other.name == "NPC_agricultura2"){
			Dialogos3.instDialogo3.ayuda2 ();
		}else if(other.name == "NPC_agricultura3"){
			Dialogos3.instDialogo3.introduccion2_1 ();
		}else if(other.name == "NPC_agricultura4"){
			Dialogos3.instDialogo3.dialogoFinal ();
		}else if(other.name == "TriggerCampo1Arido" || other.name == "TriggerCampo2Arido"){
			obj = other.name;
			cant++;
			if (cant == 1) {
				Dialogos4.instDialogo4.paso1Arido ();
			} else{
				if(Dialogos4.instDialogo4.resultados == false){
					Dialogos4.instDialogo4.paso3Arido ();
				}
			}
		}else if(other.name == "TriggerCampo1Oasis" || other.name == "TriggerCampo2Oasis" || other.name == "TriggerCampo3Oasis"){
			obj = other.name;
			cant1++;
			if (cant1 == 1) {
				Dialogos4.instDialogo4.paso1Oasis ();
			} else{
				if(Dialogos4.instDialogo4.resultados == false){
					Dialogos4.instDialogo4.paso3Oasis ();
				}
			}
		}else if(other.name == "TriggerCampo1Meso" || other.name == "TriggerCampo2Meso" || other.name == "TriggerCampo3Meso" || other.name == "TriggerCampo4Meso"){
			obj = other.name;
			cant2++;
			if (cant2 == 1) {
				Dialogos4.instDialogo4.paso1Meso ();
			} else{
				if(Dialogos4.instDialogo4.resultados == false){
					Dialogos4.instDialogo4.paso3Meso ();
				}
			}
		}else if(other.name == "TriggerAguaMeso"){
			Dialogos4.instDialogo4.paso2Meso ();
		}else if(other.name == "TriggerAgua1Oasis" || other.name == "TriggerAgua2Oasis"){
			Dialogos4.instDialogo4.paso2Oasis ();
		}else if(other.name == "TriggerAguaArido"){
			Dialogos4.instDialogo4.paso2Arido ();
		}
	}

	public void salirDialogo(){
		Time.timeScale = 1;
		panel.SetActive (false);
	}

	public void siguienteAnimal(){
		Time.timeScale = 1;
		panel.SetActive (false);
		Mapas1.instMapa.Mision1_2 ();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetoEscena : MonoBehaviour {
    public int id = -1;
    public int cantidad = 1;

	void OnTriggerEnter2D(Collider2D evento){
		if (Inventario.inv.canAdd (id)) {
			Inventario.inv.addObjeto (id);
			Destroy (gameObject);
		} else {
			if(Inventario.inv.handler.esAcumulable(id) && Inventario.inv.enInventario(id)){
				Inventario.inv.addObjeto (id);
				Destroy (gameObject);
			}
		}
	}

	void Start () {
		
	}
	
	void Update () {
		
	}
}

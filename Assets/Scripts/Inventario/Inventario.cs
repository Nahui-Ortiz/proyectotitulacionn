﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Inventario : MonoBehaviour {
    public static Inventario inv = null;
    public List<InstanciaObjeto> listaObjetos;
    public int maxNumObj = 20;
    public BaseDeDatosHandler handler = null;
    public GameObject menuObjeto;

    void Awake()
    {
        if (inv == null)
        {
            inv = this;
        }
        else if (inv != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    /* Obtiene los datos de la base de datos*/
    void Start()
    {
        if (GameManager.instancia != null)
        {
            handler = GameManager.instancia.GetComponent<BaseDeDatosHandler>();
        }
    }

    /* Se guardan los objetos del inventario en una lista*/
    void cargarInventario()
    {
        listaObjetos = new List<InstanciaObjeto>();
    }

    /* Funciona para agregar objetos al inventario*/
    public void addObjeto(int id)
    {
        if (handler.existeObjeto(id)) //Revisa si el objeto existe en la base de datos
        {
            if (handler.esAcumulable(id) && enInventario(id)) //Revisa si el objeto es acumulable
            {
                listaObjetos.Find(objeto => objeto.id == id).cantidad++; //Si lo es, busca el objeto en el inventario y añade uno
            }
            else if(canAdd(id)) //Si no es acumulable revisa si puede agregar otro objeto al inventario
            {
                listaObjetos.Add(new InstanciaObjeto(id, 1)); //Agrega el objeto al inventario
            }
            else
            {
                Debug.Log("Limite maximo alcanzado"); //Si no puede agregar es porque el cupo del inventario se llenó
            }
        }
        else
        {
            Debug.Log("ERROR: No se pudo añadir el objeto"); //Si el objeto no existe no se puede añadir
        }
    }

    /* Funcion para borrar objeto del inventario*/
    public void borrarObjeto(int id)
    {
        if (enInventario(id)) //Revisa si existe en el inventario
        {
            InstanciaObjeto obj = listaObjetos.Find(objeto => objeto.id == id); //Busca el objeto seleccionado

            if (obj.cantidad > 1) //Si el objeto es acumulable y tiene mas de 1 guardado
            {
                obj.cantidad--; //Elimina solo un objeto del objeto acumulable
            }
            else //Si no es acumulable o solo tiene uno 
            {
                listaObjetos.Remove(listaObjetos.Find(x => x.id == id)); //Lo elimina del inventario
            }
        }
    }

	internal void borrarObjetoPorPosicion(int posicion){
		if(posicion < listaObjetos.Count){
			listaObjetos.Remove (listaObjetos[posicion]);
		}
	}

    /* Retorna si un objeto existe en el inventario*/
    public bool enInventario(int id)
    {
        return listaObjetos.Exists(x => x.id == id);
    }

    /* Funcion para guardar el estado del inventario
     * Utiliza un archivo json*/
    public void guardarEstado()
    {
        string inventarioStrinified = JsonUtility.ToJson(this);
        File.WriteAllText(Application.dataPath + "/Resources/inventario.json", inventarioStrinified);
    }

    /* Funcion para cargar el estado guardado del inventario
     * Muestra la informacion del archivo json donde se guardo el estado */
    public void cargarEstado()
    {
        if (File.Exists(Application.dataPath + "/Resources/inventario.json"))
        {
            string datos = File.ReadAllText(Application.dataPath + "/Resources/inventario.json");
            JsonUtility.FromJsonOverwrite(datos, this);
        }
    }

    /* Retorna si la cantidad de objetos del inventario es menor al numero maximo*/
    public bool canAdd(int id)
    {
        return this.listaObjetos.Count < this.maxNumObj;
    }

    
    public void activarMenuObjeto(int id, int posicion, RectTransform rectT, Vector2 eventPos, GameObject objetoUI)
    {
        if (menuObjeto == null)
        {
            menuObjeto = GameManager.instancia.inventarioUI.transform.GetChild(0).Find("MenuObjeto").gameObject;
        }
		ObjetoInfo menu = menuObjeto.GetComponent<ObjetoInfo> ();
		menu.id = id;
		menu.posicion = posicion;
		menu.objetoUI = objetoUI;

        Rect rec = RectTransformUtility.PixelAdjustRect(rectT, GameManager.instancia.inventarioUI.GetComponent<Canvas>());

        float offsetX = this.menuObjeto.GetComponent<RectTransform>().rect.width / 2;
        float offsetY = this.menuObjeto.GetComponent<RectTransform>().rect.height / 2;

        Vector3 pos = new Vector3(eventPos.x + offsetX, eventPos.y - offsetY);
        this.menuObjeto.GetComponent<RectTransform>().position = pos;
        this.menuObjeto.SetActive(true);
    }
}

/* Clase para instanciar los objetos del inventario*/
[System.Serializable]
public class InstanciaObjeto{
    public int id;
    public int cantidad;

    public InstanciaObjeto(int objID = -1, int objCant = 0)
    {
        id = objID;
        cantidad = objCant;
    }
}

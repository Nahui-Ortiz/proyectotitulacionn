﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CustomEditor(typeof(Inventario))]
public class InventarioEditor : Editor {
	int indice = 0;

	/* Parte grafica para añadir y eliminar objetos del inventario*/
	public override void OnInspectorGUI(){
		Inventario contexto = (Inventario)this.target;

		if (GameManager.instancia != null){
			BaseDeDatosHandler bd2 = GameManager.instancia.GetComponent<BaseDeDatosHandler>();
			string[] opciones = new string[bd2.bd.baseDatos.Count];
			Dictionary<string, int> diccionario = new Dictionary<string, int>();

			foreach (Objeto obj in bd2.bd.baseDatos)
			{
				diccionario.Add(obj.nombre, obj.id);
			}

			/* Muestra el numero maximo que puede tener el inventario*/
			diccionario.Keys.CopyTo(opciones, 0);
			EditorGUILayout.LabelField("Numero de objetos:", contexto.listaObjetos.Count.ToString() + "/" + contexto.maxNumObj);   
			EditorGUILayout.LabelField("Objeto", "Cantidad");

			/* Muestra los objetos que hay en el inventario*/
			foreach (InstanciaObjeto obj in contexto.listaObjetos)
			{
				Objeto objActual = bd2.buscarObjetoPorID(obj.id);
				EditorGUILayout.LabelField(objActual.nombre, obj.cantidad.ToString());
			}

			/* Selector del objeto para añadir o eliminar del inventario*/
			indice = EditorGUILayout.Popup(indice, opciones);

			/* Boton para añadir objetos*/
			if (GUILayout.Button("Añadir objeto"))
			{
				int idObjeto;
				if (diccionario.TryGetValue(opciones[indice], out idObjeto))
				{
					contexto.addObjeto(idObjeto);
				}
			}

			/* Boton para eliminar objetos*/
			if (GUILayout.Button("Borrar objeto"))
			{
				int idObjeto;
				if (diccionario.TryGetValue(opciones[indice], out idObjeto))
				{
					contexto.borrarObjeto(idObjeto);
				}
			}
		}
	}
}

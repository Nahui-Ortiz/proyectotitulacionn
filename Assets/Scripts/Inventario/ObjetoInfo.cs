﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjetoInfo : MonoBehaviour {
    public int id;
    public int posicion;
    public GameObject objetoUI;
    public GameObject objetoEscenaPrefab;
	
    public void soltarObjeto()
    {
		Inventario.inv.borrarObjetoPorPosicion (posicion);
		GameObject objeto = Instantiate (objetoEscenaPrefab);
		objeto.name = "objeto_" + id.ToString();
		Sprite sprite = new Sprite();
		if (GameManager.instancia.spritesObjeto.TryGetValue (id, out sprite)) {
			objeto.GetComponent<SpriteRenderer> ().sprite = sprite;
		} else {
			Debug.Log ("El sprite no existe");
		}
		objeto.GetComponent<ObjetoEscena> ().id = id;
		Destroy (objetoUI);
		this.gameObject.SetActive (false);
    }

	void Start () {
		
	}
	
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Objeto : ISerializationCallbackReceiver {
    public int id;
    public string nombre;
    public string tipo;
    public string nombreSprite;
    public bool acumulable;
    public string rutaSprite;


    void ISerializationCallbackReceiver.OnBeforeSerialize(){}
    void ISerializationCallbackReceiver.OnAfterDeserialize(){
        this.rutaSprite = "Sprites/" + nombreSprite;
    }
}

/* Clase donde se guarda la lista de los objetos de la base de datos*/
[System.Serializable]
public class BaseDeDatos{
	public List<Objeto> baseDatos;
}
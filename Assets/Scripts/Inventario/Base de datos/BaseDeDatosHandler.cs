﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class BaseDeDatosHandler : MonoBehaviour {
    public BaseDeDatos bd;

    /* Lee el archivo json de la base de datos*/
    void Start(){
        string datos = File.ReadAllText(Application.dataPath + "/Resources/objetos.json");
        bd = JsonUtility.FromJson<BaseDeDatos>(datos);
    }

    /* Funcion para buscar un objeto mediante su id*/
    public Objeto buscarObjetoPorID(int id){
        return bd.baseDatos.Find(objeto => objeto.id == id);
    }

    /* Retorna si existe el objeto en la base de datos*/
    public bool existeObjeto(int id){
        return bd.baseDatos.Exists(objeto => objeto.id == id);
    }

    /* Retorna si un objeto es acumulable*/
    public bool esAcumulable(int id){
        Objeto obj = bd.baseDatos.Find(objeto => objeto.id == id);
        return obj.acumulable;
    }
}

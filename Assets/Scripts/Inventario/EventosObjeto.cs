﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventosObjeto : MonoBehaviour, IPointerClickHandler{
    public int id;
    public int posicion;
	
	void Start () {
		
	}
	
	void Update () {
		
	}

	public void OnPointerClick(PointerEventData eventData){
		if (eventData.button.Equals(PointerEventData.InputButton.Right))
		{
			Inventario.inv.activarMenuObjeto(id, posicion, this.GetComponent<RectTransform>(), eventData.position, this.gameObject);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CargarObjetos : MonoBehaviour {
    public GameObject objeto;
    private Inventario inventario;
    private BaseDeDatosHandler handler;

	/* Muestra el inventario instanciado*/
	void Start () {
        inventario = Inventario.inv;
        handler = GameManager.instancia.GetComponent<BaseDeDatosHandler>();
        this.pintaObjetos();
	}
	
	
	void Update () {
		
	}

    /* Funcion para "pintar" el inventario*/
    void pintaObjetos(){
		int posicion = 0;
        foreach (InstanciaObjeto obj in inventario.listaObjetos){
            GameObject instanciaObjeto = Instantiate(objeto);
			EventosObjeto ev = instanciaObjeto.GetComponent<EventosObjeto> ();
			ev.id = obj.id;
			ev.posicion = posicion;

            Objeto detalleObjeto = handler.buscarObjetoPorID(obj.id);
			instanciaObjeto.transform.GetComponentInChildren<Text>().text = detalleObjeto.nombre;
            instanciaObjeto.transform.SetParent(this.transform);

			posicion++;
        }
    }
}

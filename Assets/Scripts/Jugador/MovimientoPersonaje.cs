﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPersonaje : MonoBehaviour {
	Animator anim;

    /* Obtiene el componente Animator del personaje
     * en una variable*/
	void Start () {
		anim = GetComponent<Animator> ();
	}

    /* Controla la animacion del personaje conforme el movimiento*/
	void Update () {
		Vector2 movement_vector = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));
		if (movement_vector != Vector2.zero) {
			anim.SetBool ("IsWalking", true);
			anim.SetFloat ("Input_x", movement_vector.x);
			anim.SetFloat ("Input_y", movement_vector.y);
		} else {
			anim.SetBool ("IsWalking", false);
		}
	}
}

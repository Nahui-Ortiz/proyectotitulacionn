﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoCamara : MonoBehaviour {
	public Transform target;
	public float m_speed = 0.2f;

    /* Funcion para */
	void Update () {
		if (target) {
			transform.position = Vector3.Lerp (transform.position, target.position, m_speed)+(new Vector3(0,0,-10));
		}
	}
}

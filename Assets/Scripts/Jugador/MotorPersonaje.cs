﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorPersonaje : MonoBehaviour {
	private Rigidbody2D rbody;

    /* Obtiene el componente Rigibody del personaje
     * en una variable*/
    void Start(){
		rbody = GetComponent<Rigidbody2D> ();
	}

    /* Maneja el movimiento (direccion) del personaje*/
	void Update () {
		Vector2 movement_vector = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical"));
		rbody.MovePosition (rbody.position + movement_vector * Time.deltaTime);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SeleccionPersonaje : MonoBehaviour {

	private GameObject[] personajes;
	private int indice;

    
	private void Start(){
		indice = PlayerPrefs.GetInt ("personaje");
		personajes = new GameObject[transform.childCount];

		for(int i = 0; i < transform.childCount; i++){
			personajes [i] = transform.GetChild (i).gameObject;
		}
		foreach (GameObject go in personajes) {
			go.SetActive (false);	
		}
		if (personajes [indice]) {
			personajes [indice].SetActive (true);
		}
	}

    /* Funcion para el boton izquierdo
     * */
	public void seleccionIzq(){
		personajes [indice].SetActive (false);
		indice--;
		if (indice < 0) {
			indice = personajes.Length - 1;
		}
		personajes [indice].SetActive (true);
	}

    /* Funcion para el boton derecho
     * */
    public void seleccionDer(){
		personajes [indice].SetActive (false);
		indice++;
		if (indice == personajes.Length) {
			indice = 0;
		}
		personajes [indice].SetActive (true);
	}

	public void comenzar(){
		PlayerPrefs.SetInt ("personaje",indice);
		SceneManager.LoadScene ("Arco1");
	}
}

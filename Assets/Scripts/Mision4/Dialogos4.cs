﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogos4 : MonoBehaviour {
	public static Dialogos4 instDialogo4 = null;
	public Text outputF;
	public GameObject panelDialogo, btnSalir, btnSiguiente, btnSi;
	public GameObject panelSeleccion, op1, op2, op3;
	public Button siguiente, si;
	public Button btnOp1, btnOp2, btnOp3;
	public Text opc1, opc2, opc3;
	string plantas = "", campo = "";
	public bool paso1 = false, paso2 = false, paso3 = false, paso4 = false;
	public bool resultados = false, iniTiempo = false;
	public bool aridoAmerica = false, mesoAmerica = false, oasisAmerica = false;
	public float tiempo = 30f;

	// Use this for initialization
	void Start () {
		instDialogo4 = this;
		siguiente = btnSiguiente.GetComponent<Button> ();
		si = btnSi.GetComponent<Button> ();
		btnOp1 = op1.GetComponent<Button> ();
		btnOp2 = op2.GetComponent<Button> ();
		btnOp3 = op3.GetComponent<Button> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (iniTiempo == true){
			tiempo -= Time.deltaTime;
			if (tiempo <= 0) {
				resultadosArido ();
				iniTiempo = false;
			}
		}
	}
		
	void panelSi(){
		Time.timeScale = 0;
		btnSi.SetActive (true);
		btnSalir.SetActive (true);
		btnSiguiente.SetActive (false);
		panelDialogo.SetActive (true);
	}

	void panelSiguiente(){
		Time.timeScale = 0;
		btnSi.SetActive (false);
		btnSalir.SetActive (false);
		btnSiguiente.SetActive (true);
		panelDialogo.SetActive (true);
	}

	void panelAmbos(){
		Time.timeScale = 0;
		btnSi.SetActive (false);
		btnSalir.SetActive (true);
		btnSiguiente.SetActive (true);
		panelDialogo.SetActive (true);
	}

	void panelSalir(){
		Time.timeScale = 0;
		btnSi.SetActive (false);
		btnSalir.SetActive (true);
		btnSiguiente.SetActive (false);
		panelDialogo.SetActive (true);
	}

	void panelNo(){
		Time.timeScale = 0;
		btnSi.SetActive (false);
		btnSalir.SetActive (false);
		btnSiguiente.SetActive (false);
		panelDialogo.SetActive (true);
	}

	void ini(){
		paso1 = false;
		paso2 = false;
		paso3 = false;
		resultados = false;
		tiempo = 30f;
	}

	public void noPermitido(){
		outputF.text = "Ya has jugado esta zona.";
		panelSalir ();
	}

	void sel(){
		Mapas1.instMapa.Mision4 ();
		panelDialogo.SetActive (false);
	}

	public void introAridoamerica(){
		ini ();
		aridoAmerica = true;
		outputF.text = "Aridoamerica se caracteriza por tener zonas aridas y semiaridas, asi como montañas, mesetas, estepas, desiertos y "
			+ "costas. Su clima es muy caluroso duarnte el día y frio durante la noche.";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (introAridoamerica2);
	}

	void introAridoamerica2(){
		outputF.text = "Este lugar es simplemente dificil de habitar. ¡Has elegido el lugar mas dificil para cultivar! Primero elige las plantas"
			+ " que deseas plantar.";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (seleccionPlantas);
	}

	public void introOasisamerica(){
		ini ();
		oasisAmerica = true;
		outputF.text = "Oasisamerica tiene un terreno semiarido y clima caluroso con escasas lluvias, pero cuenta con algunas tierras favorables "
			+ "para la agricultura debido a la presencia de oasis.";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (introOasisamerica2);
	}

	void introOasisamerica2(){
		outputF.text = "Tendrás que sembrar plantas aquí, usando lo que los nomadas y habitantes de oasisamerica usaban. Esta area tiene un nivel "
			+ "de dificultad medio.";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (seleccionPlantas);
	}

	public void introMesoamerica(){
		ini ();
		mesoAmerica = true;
		outputF.text = "Mesoamerica se distingue por tener bosques, selvas, costas y montañas, varios tipos de climas y abundancia de rios y lagunas "
			+ "en ciertas zonas, lo que favoreció el desarrollo de la agricultura.";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (introMesoamerica2);
	}

	void introMesoamerica2(){
		outputF.text = "Tendrás que sembrar plantas aquí, haciendo lo que los nomadas y habitantes de oasisamerica usaban. Esta area tiene un nivel "
			+ "de dificultad bajo. ";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (seleccionPlantas);
	}

	void seleccionPlantas(){
		opc1.text = "Maiz";
		opc2.text = "Chile";
		opc3.text = "Frijol";
		panelSeleccion.SetActive (true);
		panelNo ();
		btnOp1.onClick.RemoveAllListeners();
		btnOp1.onClick.AddListener (plantas1);
		btnOp2.onClick.RemoveAllListeners();
		btnOp2.onClick.AddListener (plantas2);
		btnOp3.onClick.RemoveAllListeners();
		btnOp3.onClick.AddListener (plantas3);
	}

	void plantas1(){
		plantas = "maiz";
		introGeneral ();
	}

	void plantas2(){
		plantas = "chile";
		introGeneral ();
	}

	void plantas3(){
		plantas = "frijol";
		introGeneral ();
	}

	void introGeneral(){
		panelSeleccion.SetActive (false);
		outputF.text = "Bien, ahora que has elegido " + plantas + ", busca el lugar para cosecharlo.";
		panelSalir ();
	}

	public void paso1Meso(){
		campo = Detector.instDetector.obj;
		paso1 = true;
		outputF.text = "Bien, este es un lugar bueno para cultivar, primero hay que poner las semillas.";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (paso1_2Meso);
	}

	public void paso1_2Meso(){
		outputF.text = "¡Listo! Plantaste las semillas, recuerda que puedes regarlas con agua y esta puedes buscarla cerca de aquí. Dentro de "
			+ "1 minuto podrás saber si tu cultivo tuvo exito.";
		panelSalir ();
		iniTiempo = true;
	}

	public void paso2Meso(){
		paso2 = true;
		outputF.text = "Has encontrado agua.";
		panelSalir ();
	}

	public void paso3Meso(){
		if(Detector.instDetector.obj == campo){
			outputF.text = "¿Quieres regar tu cultivo? (Si no quieres, presiona salir.)";
			panelAmbos ();
			siguiente.onClick.RemoveAllListeners ();
			siguiente.onClick.AddListener (paso3_2Meso);
		}
	}

	void paso3_2Meso(){
		if (paso2 == true) {
			paso3 = true;
			panelDialogo.SetActive (false);
			Time.timeScale = 1;
		} else {
			outputF.text = "Al parecer no tienes agua, ve a buscarla.";
			panelSalir ();
		}
	}

	void resultadosMeso(){
		resultados = true;
		int num = 0;
		num = Random.Range (1, 100);
		if (plantas == "chile") {
			if (num >= 30) {
				if (paso3 == true) {
					exitoMeso ();
				} else {
					if (num >= 50) {
						exitoMeso ();
					} else {
						failMeso ();
					}
				}
			} else {
				failMeso ();
			}
		} else if (plantas == "frijol") {
			if (num >= 25) {
				if (paso3 == true) {
					exitoMeso ();
				} else {
					if (num >= 40) {
						exitoMeso ();
					} else {
						failMeso ();
					}
				}
			} else {
				failMeso ();
			}
		} else if (plantas == "maiz"){
			if (num >= 10) {
				if (paso3 == true) {
					exitoMeso ();
				} else {
					if (num >= 20) {
						exitoMeso ();
					} else {
						failMeso ();
					}
				}
			} else {
				failMeso ();
			}
		}
	}

	void exitoMeso(){
		outputF.text = "¡Felicidades! Estas plantas han crecido, aunque es porque elegiste plantas que crecen mas facilmente en este clima, "
			+ "ve a otra zona y sigue probando suerte ahí.";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (sel);
	}

	void failMeso(){
		outputF.text = "Vaya, al parecer no se ha logrado cultivar algo aquí, sabemos que no es nada fácil por este clima tan dificil, ¡ve a otra zona "
			+ "y prueba suerte ahí!";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (sel);
	}

	public void paso1Arido(){
		campo = Detector.instDetector.obj;
		paso1 = true;
		outputF.text = "Bien, este es un lugar bueno para cultivar, primero hay que poner las semillas.";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (paso1_2Arido);
	}

	void paso1_2Arido(){
		outputF.text = "¡Listo! Plantaste las semillas, recuerda que puedes regarlas con agua y esta puedes buscarla cerca de aquí. Dentro de "
			+ "1 minuto podrás saber si tu cultivo tuvo exito.";
		panelSalir ();
		iniTiempo = true;
	}

	public void paso2Arido(){
		paso2 = true;
		outputF.text = "Has encontrado agua.";
		panelSalir ();
	}

	public void paso3Arido(){
		if(Detector.instDetector.obj == campo){
			outputF.text = "¿Quieres regar tu cultivo? (Si no quieres, presiona salir.)";
			panelAmbos ();
			siguiente.onClick.RemoveAllListeners ();
			siguiente.onClick.AddListener (paso3_2Arido);
		}
	}

	void paso3_2Arido(){
		if (paso2 == true) {
			paso3 = true;
			panelDialogo.SetActive (false);
			Time.timeScale = 1;
		} else {
			outputF.text = "Al parecer no tienes agua, ve a buscarla.";
			panelSalir ();
		}
	}

	void resultadosArido(){
		resultados = true;
		int num = 0;
		num = Random.Range (1, 100);
		if (plantas == "maiz" || plantas == "chile") {
			failArido ();
		} else if (plantas == "frijol"){
			if (num >= 80 && paso3 == true) {
				exitoArido ();
			} else {
				failArido ();
			}
		}
	}

	void exitoArido(){
		outputF.text = "¡Felicidades! Milagrosamente estas plantas han crecido, aunque es porque elegiste plantas que crecen mas facilmente en este clima, "
			+ "ve a otra zona y sigue probando suerte ahí.";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (sel);
	}

	void failArido(){
		outputF.text = "Vaya, al parecer no se ha logrado cultivar algo aquí, sabemos que no es nada fácil por este clima tan dificil, ¡ve a otra zona "
			+ "y prueba suerte ahí!";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (sel);
	}

	public void paso1Oasis(){
		campo = Detector.instDetector.obj;
		paso1 = true;
		outputF.text = "Bien, este es un lugar bueno para cultivar, primero hay que poner las semillas.";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (paso1_2Oasis);
	}

	public void paso1_2Oasis(){
		outputF.text = "¡Listo! Plantaste las semillas, recuerda que puedes regarlas con agua y esta puedes buscarla cerca de aquí. Dentro de "
			+ "1 minuto podrás saber si tu cultivo tuvo exito.";
		panelSalir ();
		iniTiempo = true;
	}

	public void paso2Oasis(){
		paso2 = true;
		outputF.text = "Has encontrado agua.";
		panelSalir ();
	}

	public void paso3Oasis(){
		if(Detector.instDetector.obj == campo){
			outputF.text = "¿Quieres regar tu cultivo? (Si no quieres, presiona salir.)";
			panelAmbos ();
			siguiente.onClick.RemoveAllListeners ();
			siguiente.onClick.AddListener (paso3_2Oasis);
		}
	}

	void paso3_2Oasis(){
		if (paso2 == true) {
			paso3 = true;
			panelDialogo.SetActive (false);
			Time.timeScale = 1;
		} else {
			outputF.text = "Al parecer no tienes agua, ve a buscarla.";
			panelSalir ();
		}
	}

	void resultadosOasis(){
		resultados = true;
		int num = 0;
		num = Random.Range (1, 100);
		if (plantas == "chile") {
			failOasis ();
		} else if (plantas == "frijol") {
			if (num >= 30) {
				if (paso3 == true) {
					exitoOasis ();
				} else {
					if (num >= 50) {
						exitoOasis ();
					} else {
						failOasis ();
					}
				}
			} else {
				failOasis ();
			}
		} else if (plantas == "maiz"){
			if (num >= 10) {
				if (paso3 == true) {
					exitoOasis ();
				} else {
					if (num >= 40) {
						exitoOasis ();
					} else {
						failOasis ();
					}
				}
			} else {
				failOasis ();
			}
		}
	}

	void exitoOasis(){
		outputF.text = "¡Felicidades! Estas plantas han crecido, aunque es porque elegiste plantas que crecen mas facilmente en este clima, "
			+ "ve a otra zona y sigue probando suerte ahí.";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (sel);
	}

	void failOasis(){
		outputF.text = "Vaya, al parecer no se ha logrado cultivar algo aquí, puedes volver a intentarlo si quieres o ve a otra zona "
			+ "y prueba suerte ahí.";
		panelSiguiente ();
		siguiente.onClick.RemoveAllListeners ();
		siguiente.onClick.AddListener (sel);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mapas1 : MonoBehaviour {
	public static Mapas1 instMapa = null;
	public GameObject jugadorObjeto;
	public GameObject mapaUI;
	//Mision 1
	public GameObject prefabMapa1, prefabMapa2, ctrl1;
	//Mision 2
	public GameObject prefabMapa3, ctrl2;
	//Mision 3
	public GameObject prefabMapa4, prefabMapa5, ctrl3;
	//Mision4 
	public GameObject prefabMapa6, prefabMapa7, prefabMapa8, prefabMapa9, seleccion;

	// Use this for initialization
	void Start () {
		instMapa = this;
		Mision1_1 ();
		//Mision2 ();
		//Mision3_1();
		//Mision4 ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void destruyeActual(){
		Destroy (mapaUI);
	}

	public void Mision1_1(){
		Instantiate(ctrl1);
		mapaUI = Instantiate (prefabMapa1);
	}

	public void Mision1_2(){
		var jugador = GameObject.Find("Player");
		Destroy (mapaUI);
		InstanciaAnimales.instAnimales.destroyIniciales ();
		mapaUI = Instantiate (prefabMapa2, jugador.transform.position, Quaternion.identity);
	}

	public void Mision2(){
		//Instantiate(ctrl2);
		var jugador = GameObject.Find ("Player");
		mapaUI = Instantiate (prefabMapa3, jugador.transform.position, Quaternion.identity);
	}

	public void Mision3_1(){
		//Instantiate(ctrl3);
		Destroy (mapaUI);
		var jugador = GameObject.Find ("Player");
		mapaUI = Instantiate (prefabMapa4, jugador.transform.position, Quaternion.identity);
	}

	public void Mision3_2(){
		Destroy (mapaUI);
		var jugador = GameObject.Find ("Player");
		mapaUI = Instantiate (prefabMapa5, jugador.transform.position, Quaternion.identity);
	}

	public void Mision4(){
		Destroy (mapaUI);
		seleccion.SetActive (true);
		jugadorObjeto.SetActive (false);
	}

	public void Mision4_1(){
		if(Dialogos4.instDialogo4.oasisAmerica == false){
			jugadorObjeto.SetActive (true);
			seleccion.SetActive (false);
			Destroy (mapaUI);
			var jugador = GameObject.Find ("Player");
			mapaUI = Instantiate (prefabMapa7, jugador.transform.position, Quaternion.identity);
			Dialogos4.instDialogo4.introOasisamerica ();
		}else{
			Dialogos4.instDialogo4.noPermitido ();
		}
	}

	public void Mision4_2(){
		if(Dialogos4.instDialogo4.aridoAmerica == false){
			jugadorObjeto.SetActive (true);
			seleccion.SetActive (false);
			Destroy (mapaUI);
			var jugador = GameObject.Find ("Player");
			mapaUI = Instantiate (prefabMapa8, jugador.transform.position, Quaternion.identity);
			Dialogos4.instDialogo4.introAridoamerica ();
		}else{
			Dialogos4.instDialogo4.noPermitido ();
		}
	}

	public void Mision4_3(){
		if(Dialogos4.instDialogo4.mesoAmerica == false){
			jugadorObjeto.SetActive (true);
			seleccion.SetActive (false);
			Destroy (mapaUI);
			var jugador = GameObject.Find ("Player");
			mapaUI = Instantiate (prefabMapa9, jugador.transform.position, Quaternion.identity);
			Dialogos4.instDialogo4.introMesoamerica ();
		}else{
			Dialogos4.instDialogo4.noPermitido ();
		}
	}
}

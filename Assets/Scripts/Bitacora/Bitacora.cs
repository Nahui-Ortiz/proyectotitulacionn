﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Bitacora : MonoBehaviour {
	public InputField inputF;
	public Text outputF;

	void Start () {
		string inicio;
		inicio = File.ReadAllText(Application.dataPath + "/Resources/prueba.txt");
		outputF.text = inicio;
	}

	public void ObtenerEntrada () {
		string lectura;
		lectura = File.ReadAllText(Application.dataPath + "/Resources/prueba.txt");

		string entrada = inputF.text;
		string bitacora = lectura + "\n" + entrada;
		File.WriteAllText (Application.dataPath + "/Resources/prueba.txt", bitacora);

		inputF.text = "";
		ActualizaTexto ();
	}

	void ActualizaTexto(){
		string texto;
		texto = File.ReadAllText (Application.dataPath + "/Resources/prueba.txt");
		outputF.text = texto;
	}
}

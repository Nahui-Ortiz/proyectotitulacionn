﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Oportunidades : MonoBehaviour {
	public static Oportunidades instOp = null;
	public Text oportunidades;

	// Use this for initialization
	void Start () {
		instOp = this;
	}
	
	// Update is called once per frame
	void Update () {
		oportunidades.text = Dialogos3.instDialogo3.oportunidades.ToString();
	}
}

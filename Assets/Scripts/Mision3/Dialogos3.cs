﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogos3 : MonoBehaviour {
	public static Dialogos3 instDialogo3 = null;
	public Text outputF;
	public GameObject panelDialogos, btnSiguiente, btnSalir, btnSi;
	Button btnSig, btnS;
	string campo;
	public bool estadoMaiz = false, estadoAguacate = false, estadoChile = false, estadoAmaranto = false, estadoFrijol = false;
	public int oportunidades = 3;
	bool d = false, e = false;

	// Use this for initialization
	void Start () {
		instDialogo3 = this;
		btnSig = btnSiguiente.GetComponent<Button> ();
		btnS = btnSi.GetComponent<Button> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void panelSiguiente(){
		Time.timeScale = 0;
		panelDialogos.SetActive (true);
		btnSiguiente.SetActive (true);
		btnSalir.SetActive (false);
		btnSi.SetActive (false);
	}

	void panelSalir(){
		Time.timeScale = 0;
		panelDialogos.SetActive (true);
		btnSiguiente.SetActive (false);
		btnSalir.SetActive (true);
		btnSi.SetActive (false);
	}

	void panelAmbosSi(){
		Time.timeScale = 0;
		panelDialogos.SetActive (true);
		btnSiguiente.SetActive (false);
		btnSi.SetActive (true);
		btnSalir.SetActive (true);
	}

	public void introduccion1(){
		if(d == false){
			d = true;
			outputF.text = "Hola, ¿que te trae por aquí? En mi pueblo yo soy el encargado junto con otras personas de sembrar plantas para comer. Hace ya "
				+ "un tiempo que encontramos la manera de producir nuestra propia comida.";
			panelSiguiente ();
			btnSig.onClick.RemoveAllListeners();
			btnSig.onClick.AddListener (introduccion2);
		}
	}

	public void introduccion2(){
		outputF.text = "Lo que sembrabamos es maiz, chile, aguacate, amaranto y frijol. Para pasar esta misión deberás identificar "
			+ "las plantas que te he mencionado que se cultivaban.";
		panelSiguiente ();
		btnSig.onClick.RemoveAllListeners();
		btnSig.onClick.AddListener (introduccion3);
	}

	public void introduccion3(){
		outputF.text = "Sino recuerdas algo, puedes hablar con alguno de los encargados de la siembra, solo recuerda no abusar mucho de esta ventaja."
			+ " Solo tienes 3 oportunidades para equivocarte al elegir las plantas y para ir con el encargado.";
		panelSalir ();
	}

	public void ayuda1(){
		outputF.text = "Hola, ¿quieres que te ayude con algo? Puedo decirte algunas plantas que cultivabamos";
		panelAmbosSi ();
		btnS.onClick.RemoveAllListeners();
		btnS.onClick.AddListener (siguienteAyuda1);
	}

	public void ayuda2(){
		outputF.text = "Hola, ¿quieres que te ayude con algo? Puedo decirte algunas plantas que cultivabamos";
		panelAmbosSi ();
		btnS.onClick.RemoveAllListeners();
		btnS.onClick.AddListener (siguienteAyuda2);
	}

	public void siguienteAyuda1(){
		oportunidades--;
		if (oportunidades == 0) {
			oportunidades = 3;
			estadoMaiz = false; 
			estadoAguacate = false; 
			estadoChile = false; 
			estadoAmaranto = false; 
			estadoFrijol = false;
			outputF.text = "Se han agotado tus oportunidades y hemos tenido que reiniciar la misión. ¡Vuelve a intentarlo!";
			panelSalir ();
			Mapas1.instMapa.Mision3_1 ();
		} else {
			outputF.text = "Claro, aquí sembrábamos diferentes plantas, como el maíz o el frijol.";
			panelSalir ();
		}
	}

	public void siguienteAyuda2(){
		oportunidades--;
		if (oportunidades == 0) {
			oportunidades = 3;
			estadoMaiz = false; 
			estadoAguacate = false; 
			estadoChile = false; 
			estadoAmaranto = false; 
			estadoFrijol = false;
			outputF.text = "Se han agotado tus oportunidades y hemos tenido que reiniciar la misión. ¡Vuelve a intentarlo!";
			panelSalir ();
			Mapas1.instMapa.Mision3_1 ();
		} else {
			outputF.text = "Sí, el chile, aguacate y amaranto también eran sembrados en estos lugares.";
			panelSalir ();
		}
	}

	public void maiz(){
		campo = "CampoMaiz";
		outputF.text = "Esto es maiz, ¿se cultivaba por los primeros habitantes del actual territorio mexicano?";
		panelAmbosSi ();
		btnS.onClick.RemoveAllListeners();
		btnS.onClick.AddListener (confirmacion);
	}

	public void zanahoria(){
		campo = "CampoZanahoria";
		outputF.text = "Esto es zanahoria, ¿se cultivaba por los primeros habitantes del actual territorio mexicano?";
		panelAmbosSi ();
		btnS.onClick.RemoveAllListeners();
		btnS.onClick.AddListener (confirmacion);
	}

	public void aguacate(){
		campo = "CampoAguacate";
		outputF.text = "Esto es aguacate, ¿se cultivaba por los primeros habitantes del actual territorio mexicano?";
		panelAmbosSi ();
		btnS.onClick.RemoveAllListeners();
		btnS.onClick.AddListener (confirmacion);
	}

	public void uva(){
		campo = "CampoUva";
		outputF.text = "Esto es uva, ¿se cultivaba por los primeros habitantes del actual territorio mexicano?";
		panelAmbosSi ();
		btnS.onClick.RemoveAllListeners();
		btnS.onClick.AddListener (confirmacion);
	}

	public void cebada(){
		campo = "CampoCebada";
		outputF.text = "Esto es cebada, ¿se cultivaba por los primeros habitantes del actual territorio mexicano?";
		panelAmbosSi ();
		btnS.onClick.RemoveAllListeners();
		btnS.onClick.AddListener (confirmacion);
	}

	public void chile(){
		campo = "CampoChile";
		outputF.text = "Esto es chile, ¿se cultivaba por los primeros habitantes del actual territorio mexicano?";
		panelAmbosSi ();
		btnS.onClick.RemoveAllListeners();
		btnS.onClick.AddListener (confirmacion);
	}

	public void frijol(){
		campo = "CampoFrijol";
		outputF.text = "Esto es frijol, ¿se cultivaba por los primeros habitantes del actual territorio mexicano?";
		panelAmbosSi ();
		btnS.onClick.RemoveAllListeners();
		btnS.onClick.AddListener (confirmacion);
	}

	public void col(){
		campo = "CampoCol";
		outputF.text = "Esto es col, ¿se cultivaba por los primeros habitantes del actual territorio mexicano?";
		panelAmbosSi ();
		btnS.onClick.RemoveAllListeners();
		btnS.onClick.AddListener (confirmacion);
	}

	public void amaranto(){
		campo = "CampoAmaranto";
		outputF.text = "Esto es amaranto, ¿se cultivaba por los primeros habitantes del actual territorio mexicano?";
		panelAmbosSi ();
		btnS.onClick.RemoveAllListeners();
		btnS.onClick.AddListener (confirmacion);
	}

	public void confirmacion(){
		if (campo == "CampoMaiz") {
			if (estadoAguacate == true && estadoChile == true && estadoAmaranto == true && estadoFrijol == true) {
				victoria ();
			}else {
				Time.timeScale = 1;
				estadoMaiz = true;
				var campo = GameObject.Find ("CampoMaiz");
				campo.GetComponent<BoxCollider2D> ().isTrigger = false; 
				panelDialogos.SetActive (false);
			}
		}else if(campo == "CampoAguacate"){
			if (estadoMaiz == true && estadoChile == true && estadoAmaranto == true && estadoFrijol == true) {
				victoria ();
			} else {
				Time.timeScale = 1;
				estadoAguacate = true;
				var campo = GameObject.Find ("CampoAguacate");
				campo.GetComponent<BoxCollider2D> ().isTrigger = false;
				panelDialogos.SetActive (false);
			}
		}else if(campo == "CampoChile"){
			if (estadoMaiz == true && estadoAguacate == true && estadoAmaranto == true && estadoFrijol == true) {
				victoria ();
			} else {
				Time.timeScale = 1;
				estadoChile = true;
				var campo = GameObject.Find ("CampoChile");
				campo.GetComponent<BoxCollider2D> ().isTrigger = false;
				panelDialogos.SetActive (false);
			}
		}else if(campo == "CampoAmaranto"){
			if (estadoMaiz == true && estadoAguacate == true && estadoChile == true && estadoFrijol == true) {
				victoria ();
			} else {
				Time.timeScale = 1;
				estadoAmaranto = true;
				var campo = GameObject.Find ("CampoAmaranto");
				campo.GetComponent<BoxCollider2D> ().isTrigger = false;
				panelDialogos.SetActive (false);
			}
		}else if(campo == "CampoFrijol"){
			if (estadoMaiz == true && estadoAguacate == true && estadoChile == true && estadoAmaranto == true) {
				victoria ();
			} else {
				Time.timeScale = 1;
				estadoFrijol = true;
				var campo = GameObject.Find ("CampoFrijol");
				campo.GetComponent<BoxCollider2D> ().isTrigger = false;
				panelDialogos.SetActive (false);
			}
		}else {
			oportunidades--;
			if (oportunidades == 0) {
				oportunidades = 3;
				estadoMaiz = false; 
				estadoAguacate = false; 
				estadoChile = false; 
				estadoAmaranto = false; 
				estadoFrijol = false;
				outputF.text = "Se han agotado tus oportunidades y hemos tenido que reiniciar la misión. ¡Vuelve a intentarlo!";
				panelSalir ();
				Mapas1.instMapa.Mision3_1 ();
			} else {
				outputF.text = "Es incorrecto. Esto no se cultivaba por los primeros habitantes del actual territorio mexicano.";
				panelSalir ();
			}
		}
	}

	void victoria(){
		outputF.text = "Buen trabajo, has elegido las plantas para cosecha correctamente, ahora deberás completar la segunda "
			+ "parte de la misión. Primero lee cómo es que descubrieron esta maravillosa actividad.";
		panelSiguiente ();
		btnSig.onClick.RemoveAllListeners();
		btnSig.onClick.AddListener (finParte1);
	}

	public void finParte1(){
		Time.timeScale = 1;
		Mapas1.instMapa.Mision3_2 ();
		panelDialogos.SetActive (false);
	}

	public void introduccion2_1(){
		e = true;
		outputF.text = "Las semillas de las plantas era dispersadas sin que las personas se dieran cuenta, ¡ni siquiera sabían lo "
			+ "que eran! Después de un tiempo se dieron cuenta, y de cómo crecían las plantas gracias a esto.";
		panelSiguiente ();
		btnSig.onClick.RemoveAllListeners();
		btnSig.onClick.AddListener (introduccion2_2);
	}

	public void introduccion2_2(){
		outputF.text = "Recolecta algunas semillas dispersadas. Oh, por cierto, si crees que ya recolectaste suficientes semillas"
			+ " ve a hablar con mi amigo, se encuentra al sur, justo donde terminan los campos. Recuerda que es importante que tengas"
			+ " algunas semillas.";
		panelSalir ();
	}

	public void dialogoFinal(){
		if(e == true){
			outputF.text = "¿Terminaste de recolectar semillas? Si es así, es hora de ir a otro lugar.";
			panelAmbosSi ();
			btnS.onClick.RemoveAllListeners();
			btnS.onClick.AddListener (finParte2);
		}
	}

	public void finParte2(){
		panelDialogos.SetActive (false);
		Mapas1.instMapa.Mision4 ();
	}
}

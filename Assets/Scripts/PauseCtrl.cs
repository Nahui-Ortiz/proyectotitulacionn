﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.IO;

public class PauseCtrl : MonoBehaviour {
	public bool pausado;
	public static GameManager instancia = null;
	public GameObject PanelMenu, PanelBoton, BotonInventario, PanelBitacora;
	private GameObject InventarioUI;
	GameManager inv;


    /* Inicializa los elementos
     * Paneles visibles o desactivados*/
	void Start () {
		pausado = false;
		PanelMenu.SetActive (false);
		PanelBoton.SetActive (true);
		BotonInventario.SetActive (false);
		PanelBitacora.SetActive (false);
	}

    /* Funcion para poner pausa
     * Desactiva los paneles de los botones
     * Activa el panel de menu*/
	public void Pausa(){
		pausado = !pausado;

		if(pausado){
			Time.timeScale = 0;
			PanelMenu.SetActive (true);
			PanelBoton.SetActive (false);
		}
	}

    /* Funcion para quitar pausa o menu
     * Vuelve a los valores iniciales*/
	public void DesPausa(){
		pausado = !pausado;

		if(!pausado){
			Time.timeScale = 1;
			PanelMenu.SetActive (false);
			PanelBoton.SetActive (true);
		}
	}

	public void AbrirBitacora(){
		if (pausado) {
			Time.timeScale = 0;
			PanelBitacora.SetActive (true);
			PanelMenu.SetActive (false);
		}
	}

	public void RegresarMenu(){
		if (pausado) {
			Time.timeScale = 0;
			PanelBitacora.SetActive (false);
			PanelMenu.SetActive (true);
		}
	}

	public void RegresarMenuInv(){
		if (pausado) {
			Time.timeScale = 0;
			BotonInventario.SetActive (false);
			PanelMenu.SetActive (true);
			GameManager.instancia.toggleInventario ();
		}
	}

	public void AbrirInventario(){
		if (pausado) {
			Time.timeScale = 0;
			BotonInventario.SetActive (true);
			GameManager.instancia.toggleInventario ();
			PanelMenu.SetActive (false);
		}
	}

    /* Funcion para cargar una escena mediante un boton*/
    public void LoadScene(string sceneName)
	{
		Time.timeScale = 1;
		SceneManager.LoadScene (sceneName);
	}

}

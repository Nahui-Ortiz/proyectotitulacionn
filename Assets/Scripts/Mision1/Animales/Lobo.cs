﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lobo : MonoBehaviour {
	public static Lobo instLobo = null;
	public float posX = 0f, posY = 0f, posX1 = 0f, posY1 = 0f, posX2 = 0f, posY2 = 0f;
	Animator anim;
	Vector2 movementRight, movementLeft, movementDown, movementUp;

	// Use this for initialization
	void Start () {
		instLobo = this;
		anim = GetComponent<Animator> ();
		movementRight = new Vector2 (2f, 0);
		movementLeft = new Vector2 (-2f, 0);
		movementDown = new Vector2 (0, -2f);
		movementUp = new Vector2 (0, 2f);
		this.GetComponent<Rigidbody2D> ().velocity = movementRight;
		right ();
	}
	
	// Update is called once per frame
	void Update () {
		var ifMapIni = GameObject.Find("1_1MigrantesInicial(Clone)");
		var ifMap = GameObject.Find ("1_1MigrantesGenerico(Clone)");
		if (ifMapIni != null) {
			if (transform.position.x > 16.5f) {
				this.GetComponent<Rigidbody2D> ().velocity = movementDown;
				down ();
			}
			if (transform.position.y < -15f) {
				this.GetComponent<Rigidbody2D> ().velocity = movementLeft;
				left ();
			}
			if (transform.position.x < 1f) {
				this.GetComponent<Rigidbody2D> ().velocity = movementUp;
				up ();
			}
			if (transform.position.y > -6f) {
				this.GetComponent<Rigidbody2D> ().velocity = movementRight;
				right ();
				if (transform.position.x > 16.5f) {
					this.GetComponent<Rigidbody2D> ().velocity = movementDown;
					down ();
				}
			}
		} else if (ifMap != null) {
			posX = ifMap.transform.position.x;
			posY = ifMap.transform.position.y;
			posX1 = posX + 30f;
			posY1 = posY + -13f;
			posX2 = posX + 1f;
			posY2 = posY + -7f;
			if (transform.position.x > posX1) {
				this.GetComponent<Rigidbody2D> ().velocity = movementDown;
				down ();
			}
			if (transform.position.y < posY1) {
				this.GetComponent<Rigidbody2D> ().velocity = movementLeft;
				left ();
			}
			if (transform.position.x < posX2) {
				this.GetComponent<Rigidbody2D> ().velocity = movementUp;
				up ();
			}
			if (transform.position.y > posY2) {
				this.GetComponent<Rigidbody2D> ().velocity = movementRight;
				right ();
				if (transform.position.x > posX1) {
					this.GetComponent<Rigidbody2D> ().velocity = movementDown;
					down ();
				}
			}
		}
	}

	void up(){
		if (this.GetComponent<Rigidbody2D>().velocity != Vector2.zero){
			anim.SetFloat ("Input_x", 0);
			anim.SetFloat ("Input_y", 1f);
			anim.SetBool ("IsWalking", true);
		}
	}

	void down(){
		if (this.GetComponent<Rigidbody2D>().velocity != Vector2.zero){
			anim.SetFloat ("Input_x", 0);
			anim.SetFloat ("Input_y", -1f);
			anim.SetBool ("IsWalking", true);
		}
	}

	void left(){
		if (this.GetComponent<Rigidbody2D>().velocity != Vector2.zero){
			anim.SetFloat ("Input_x", -1f);
			anim.SetFloat ("Input_y", 0);
			anim.SetBool ("IsWalking", true);
		}
	}

	void right(){
		if (this.GetComponent<Rigidbody2D>().velocity != Vector2.zero){
			anim.SetFloat ("Input_x", 1f);
			anim.SetFloat ("Input_y", 0);
			anim.SetBool ("IsWalking", true);
		}
	}
}

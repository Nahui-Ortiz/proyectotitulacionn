﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogos1 : MonoBehaviour {
	public Text outputF;
	public GameObject panel;
	public GameObject btnSiguiente, btnSalir, btnSi;
	public int pts = 0;
	public static Dialogos1 instDialogo = null;

	// Use this for initialization
	void Start () {
		instDialogo = this;
		panel.SetActive (false);
		btnSi.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void panelSalir(){
		Time.timeScale = 0;
		btnSiguiente.SetActive (false);
		btnSalir.SetActive (true);
		panel.SetActive (true);
	}

	void panelSiguiente(){
		Time.timeScale = 0;
		btnSiguiente.SetActive (true);
		btnSalir.SetActive (false);
		panel.SetActive (true);
	}

	public void DialogoInicial(){
		outputF.text = "Hace demasiado frío, hay nieve por todos lados y lagos congelados. " +
			"Aquí puedes encontrar animales de hace miles de años, intenta buscarlos.";
		panelSalir ();
	}

	public void DialogoFinal(){
		Button btn = btnSiguiente.GetComponent<Button>();
		outputF.text = "Que curioso, ¿recuerdas el estrecho de Bering? ¿Ese lugar por donde cruzaron de Asia a América? " +
			"Se cree que las personas cruzaron aquel lugar tratando de cazar animales y así llegaron a America.";
		panelSiguiente ();
		pts = InstanciaAnimales.instAnimales.pts;
		if (pts > 0) {
			btn.onClick.RemoveAllListeners();
			btn.onClick.AddListener (siguienteSi);
		} else if (pts == 0){
			btn.onClick.RemoveAllListeners();
			btn.onClick.AddListener (siguienteNo);
		}
	}

	public void siguienteSi(){
		outputF.text = "Con los animales que cazaste has conseguido " + pts.ToString() + " puntos, te daremos algunas recompensas por tus esfuerzos, " +
			"podrás encontrar los objetos en tu inventario";
		panelSalir ();
		if (pts >= 100 && pts <= 450){
			for (int i = 0; i <= 0; i++){
				Inventario.inv.addObjeto (i);
			}
		}else if(pts >= 500 && pts <= 950){
			for (int i = 0; i <= 1; i++){
				Inventario.inv.addObjeto (i);
			}
		}else if(pts >= 1000 && pts <= 1450){
			for (int i = 0; i <= 2; i++){
				Inventario.inv.addObjeto (i);
			}
		}else if(pts >= 1500 && pts <= 1950){
			for (int i = 0; i <= 3; i++){
				Inventario.inv.addObjeto (i);
			}
		}else if(pts >= 2000 && pts <= 2200){
			for (int i = 0; i <= 4; i++){
				Inventario.inv.addObjeto (i);
			}
		}else if(pts == 2250){
			for (int i = 0; i <= 4; i++){
				Inventario.inv.addObjeto (i);
			}
			Inventario.inv.addObjeto (17);
			Inventario.inv.addObjeto (24);
		}
	}

	public void siguienteNo(){
		outputF.text = "Uy, no lograste cazar ningún animal. Pero no te preocupes, podrás avanzar a la siguiente misión sin problemas... " +
			"aunque ¡deberás mejorar para completar tus misiones y ganar el juego!";
		panelSalir ();
	}
}

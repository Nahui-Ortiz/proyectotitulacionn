﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciaAnimales : MonoBehaviour {
	public GameObject loboIni, mamutIni;
	public GameObject lobo, mamut, ciervo, bisonte, rino;
	GameObject animalLoboIni, animalMamutIni, animalLobo, animalMamut, animalCiervo, animalBisonte, animalRino;
	public static InstanciaAnimales instAnimales = null;
	int ext = 0, ext1 = 0, extLobo = 0, extMamut = 0, extCiervo = 0, extBisonte = 0, extRino = 0;
	string animal = null;
	public int pts = 0;

	// Use this for initialization
	void Start () {
		instAnimales = this;
		InvokeRepeating ("creaLoboIni", .001f, 10f);
		InvokeRepeating ("creaMamutIni", .001f, 20f);
		InvokeRepeating ("creaLobo", .001f, 10f);
		InvokeRepeating ("creaMamut", .001f, 10f);
		InvokeRepeating ("creaCiervo", .001f, 10f);
		InvokeRepeating ("creaBisonte", .001f, 10f);
		InvokeRepeating ("creaRino", .001f, 10f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void creaLoboIni(){
		var ifLoboIni = GameObject.Find("LoboIni");
		if(ext <= 2){
			if (ifLoboIni != null) {
				animalLoboIni = Instantiate (loboIni, ifLoboIni.transform.position, Quaternion.identity);
				animalLoboIni.name = "LoboIni_" + ext.ToString();
				ext++;
			}
		}
	}

	void creaMamutIni(){
		var ifMamutIni = GameObject.Find("MamutIni");
		if(ext1 <= 2){
			if (ifMamutIni != null) {
				animalMamutIni = Instantiate (mamutIni, ifMamutIni.transform.position, Quaternion.identity);
				animalMamutIni.name = "MamutIni_" + ext1.ToString ();
				ext1++;
			}
		}
	}

	public void creaLobo(){
		var ifLobo = GameObject.Find("Lobo");
		if (extLobo <= 2) {
			if (ifLobo != null) {
				animalLobo = Instantiate (lobo, ifLobo.transform.position, Quaternion.identity);
				animalLobo.name = "Lobo_" + extLobo.ToString ();
				extLobo++;
			}
		}
	}

	public void destruyeLobo(){
		animal = Detector.instDetector.obj;
		for (int i = 0; i <= 2; i++) {
			var a = GameObject.Find ("Lobo_" + i.ToString ());
			if (a != null) {
				if(a.name == animal){
					Destroy (a);
					pts += 200;
				}
			}
		}
	}

	public void creaMamut(){
		var ifMamut = GameObject.Find("Mamut");
		if (extMamut <= 2) {
			if (ifMamut != null) {
				animalMamut = Instantiate (mamut, ifMamut.transform.position, Quaternion.identity);
				animalMamut.name = "Mamut_" + extMamut.ToString ();
				extMamut++;
			}
		}
	}

	public void destruyeMamut(){
		animal = Detector.instDetector.obj;
		for (int i = 0; i <= 2; i++) {
			var b = GameObject.Find ("Mamut_" + i.ToString ());
			if (b != null) {
				if(b.name == animal){
					Destroy (b);
					pts += 150;
				}
			}
		}
	}

	public void creaCiervo(){
		var ifCiervo = GameObject.Find("Ciervo");
		if (extCiervo <= 2) {
			if (ifCiervo != null) {
				animalCiervo = Instantiate (ciervo, ifCiervo.transform.position, Quaternion.identity);
				animalCiervo.name = "Ciervo_" + extCiervo.ToString ();
				extCiervo++;
			}
		}
	}

	public void destruyeCiervo(){
		animal = Detector.instDetector.obj;
		for (int i = 0; i <= 2; i++) {
			var c = GameObject.Find ("Ciervo_" + i.ToString ());
			if (c != null) {
				if(c.name == animal){
					Destroy (c);
					pts += 100;
				}
			}
		}
	}

	public void creaBisonte(){
		var ifBisonte = GameObject.Find("Bisonte");
		if (extBisonte <= 2) {
			if (ifBisonte != null) {
				animalBisonte = Instantiate (bisonte, ifBisonte.transform.position, Quaternion.identity);
				animalBisonte.name = "Bisonte_" + extBisonte.ToString ();
				extBisonte++;
			}
		}
	}

	public void destruyeBisonte(){
		animal = Detector.instDetector.obj;
		for (int i = 0; i <= 2; i++) {
			var d = GameObject.Find ("Bisonte_" + i.ToString ());
			if (d != null) {
				if(d.name == animal){
					Destroy (d);
					pts += 150;
				}
			}
		}
	}

	public void creaRino(){
		var ifRino = GameObject.Find("Rino");
		if (extRino <= 2) {
			if (ifRino != null) {
				animalRino = Instantiate (rino, ifRino.transform.position, Quaternion.identity);
				animalRino.name = "Rino_" + extRino.ToString ();
				extRino++;
			}
		}
	}

	public void destruyeRino(){
		animal = Detector.instDetector.obj;
		for (int i = 0; i <= 2; i++) {
			var e = GameObject.Find ("Rino_" + i.ToString ());
			if (e != null) {
				if(e.name == animal){
					Destroy (e);
					pts += 150;
				}
			}
		}
	}

	public void destroyIniciales(){
		for(int i = 0; i <= 2; i++){
			var loboIni = GameObject.Find ("LoboIni_" + i.ToString());
			Destroy (loboIni);
			var mamutIni = GameObject.Find ("MamutIni_" + i.ToString());
			Destroy (mamutIni);
		}
	}

	public void destroyAnimales(){
		for(int i = 0; i <= 2; i++){
			var aLobo = GameObject.Find ("Lobo_" + i.ToString());
			Destroy (aLobo);
			var aMamut = GameObject.Find ("Mamut_" + i.ToString());
			Destroy (aMamut);
			var aCiervo = GameObject.Find ("Ciervo_" + i.ToString());
			Destroy (aCiervo);
			var aRino = GameObject.Find ("Rino_" + i.ToString());
			Destroy (aRino);
			var aBisonte = GameObject.Find ("Bisonte_" + i.ToString());
			Destroy (aBisonte);
		}
	}

	public void finMision1(){
		Destroy (gameObject);
	}
}

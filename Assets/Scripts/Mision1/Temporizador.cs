﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Temporizador : MonoBehaviour {
	public float Tiempo = 90f;
	public Text ContTiempo, ContPuntos;
	public int TiempoText, PuntosText;

	// Use this for initialization
	void Start () {
		//TemporizadorUI = Instantiate(prefabTemporizador);
	}
	
	// Update is called once per frame
	void Update () {
		ContPuntos.text = InstanciaAnimales.instAnimales.pts.ToString();
		Tiempo -= Time.deltaTime;
		TiempoText = (int)Tiempo;
		ContTiempo.text = TiempoText.ToString ();
		if(Tiempo <= 0){
			finCaza ();
		}
	}

	void finCaza(){
		Destroy (gameObject);
		InstanciaAnimales.instAnimales.destroyAnimales ();
		InstanciaAnimales.instAnimales.finMision1 ();
		Mapas1.instMapa.Mision2 ();
		Dialogos1.instDialogo.DialogoFinal ();
	}
}

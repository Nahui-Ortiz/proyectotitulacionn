﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class GameManager : MonoBehaviour {
    public static GameManager instancia = null;
	public GameObject instanciaInventario, prefabInv;
    public GameObject inventarioUI;
	public Dictionary<int, Sprite> spritesObjeto;

    void Awake()
	{
        if (instancia == null)
		{
            instancia = this;
        }
        else if (instancia != this)
		{
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        iniciaInventario();
    }

	public void cargarSpritesObjeto()
	{
		spritesObjeto = new Dictionary<int, Sprite> ();
		foreach (Objeto obj in Inventario.inv.handler.bd.baseDatos) {
			string ruta = Application.dataPath + "/Resources/" + obj.rutaSprite + ".png";

			if (File.Exists (ruta)) {
				Sprite sprite = Resources.Load<Sprite> (obj.rutaSprite);
				spritesObjeto.Add (obj.id, sprite);
			}
		}
	}

    /* Instancia el inventario en la escena*/
    void iniciaInventario(){
        if (Inventario.inv == null)
        {
            Instantiate(instanciaInventario);
        }
        Inventario.inv.cargarEstado();
    }

    public void toggleInventario()
    {
        if (Inventario.inv == null)
        {
            iniciaInventario();
        }

        if (inventarioUI == null)
        {
            Time.timeScale = 0;
            inventarioUI = Instantiate(prefabInv);
        }
        else
        {
            Destroy(inventarioUI);
            Time.timeScale = 0;
        }
    }

	void Start(){
		if (instancia != null)
		{
			Inventario.inv.handler = GetComponent<BaseDeDatosHandler>();
		}
		cargarSpritesObjeto ();
	}

	void Uptade(){

	}

    /*public void LateUpdate()
    {
        if (Input.GetButtonDown("Inventario"))
        {
			toggleInventario ();
            //InventarioMenu();
        }
    }*/

	public void RegresarMenu(){

	}

    void OnDestroy(){
        Inventario.inv.guardarEstado();
    }
}
